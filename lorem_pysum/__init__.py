"""Module for generating Pydantic model instances."""

from lorem_pysum._generator import generate

__all__ = ("generate",)
