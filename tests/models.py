"""Test models."""

from __future__ import annotations

import datetime
import typing
from _decimal import Decimal
from collections import OrderedDict
from enum import auto, Enum
from uuid import UUID, uuid4

from pydantic import (
    AllowInfNan,
    AmqpDsn,
    AnyHttpUrl,
    AnyUrl,
    AwareDatetime,
    Base64Bytes,
    Base64Str,
    BaseModel,
    ByteSize,
    CockroachDsn,
    constr,
    EmailStr,
    Field,
    FiniteFloat,
    FutureDate,
    FutureDatetime,
    HttpUrl,
    ImportString,
    IPvAnyAddress,
    IPvAnyInterface,
    IPvAnyNetwork,
    Json,
    KafkaDsn,
    MariaDBDsn,
    MongoDsn,
    MySQLDsn,
    NaiveDatetime,
    NameEmail,
    NegativeFloat,
    NegativeInt,
    NonNegativeFloat,
    NonNegativeInt,
    NonPositiveFloat,
    NonPositiveInt,
    PastDate,
    PastDatetime,
    PositiveFloat,
    PositiveInt,
    PostgresDsn,
    RedisDsn,
    SecretBytes,
    SecretStr,
    Strict,
    StrictBool,
    StrictBytes,
    StrictFloat,
    StrictInt,
    StrictStr,
    UUID1,
    UUID3,
    UUID4,
    UUID5,
)
from pydantic_extra_types.color import Color
from pydantic_extra_types.payment import PaymentCardBrand, PaymentCardNumber
from pydantic_extra_types.routing_number import ABARoutingNumber


class Flavor(Enum):
    """Coffee flavors."""

    MOCHA = auto()
    VANILLA = auto()


class Brand(BaseModel):
    """A coffee brand."""

    brand_name: str


class Coffee(BaseModel):
    """Test model."""

    id: UUID = Field(default_factory=uuid4)
    optional_str: typing.Optional[str]
    bool_field: bool
    enum_field: Flavor
    brand: Brand
    union: typing.Union[str, int, list[str]]
    always_none: None
    str_constraint_min: str = Field(min_length=101, strict=True)
    str_constraint_max: str = Field(max_length=200)
    str_constraint_minmax: str = Field(min_length=101, max_length=200)
    date_field: datetime.date
    time_field: datetime.time
    timedelta_field: datetime.timedelta
    datetime_field: datetime.datetime
    not_specifically_supported_type: OrderedDict  # type: ignore
    forward_ref_field: typing.Union[ForwardRefClass, None]
    any_type: typing.Any
    recursion: typing.Optional[Coffee]
    recursion_union: typing.Union[ForwardRefClass, Coffee]
    recursion_union_two: typing.Union[Coffee, ForwardRefClass]
    nested_models: NestedModels
    optional_default_none: typing.Optional[str] = None
    str_field: str
    bytes_field: bytes
    int_field: int
    float_field: float
    dec: Decimal
    const: typing.Literal["mocha"]


class Collections(BaseModel):
    set_any: set  # type: ignore
    set_explicit_any: set[typing.Any]
    set_int: set[int]
    tuple_any: tuple  # type: ignore
    tuple_explicit_any: tuple[typing.Any]
    tuple_int: tuple[int]
    tuple_int_str_any: tuple[int, str, typing.Any]
    list_any: list  # type: ignore
    list_explicit_any: list[typing.Any]
    list_str: list[str]
    list_of_lists: list[list[str]]
    constrained_list: list[str] = Field(min_length=10, max_length=20)
    constrained_list_union: list[typing.Union[str, int]] = Field(
        min_length=10, max_length=20
    )
    constrained_list_unique: set[int] = Field(min_length=1)
    untyped_dict: dict  # type: ignore
    dictionary: dict[str, list[int]]


class ForwardRefClass(BaseModel):
    """Class to forward ref."""

    str_field: str


class IntLtLeGtGe(BaseModel):
    # Lt
    lt_2: int = Field(lt=2.0)
    lt_1: int = Field(lt=1.0)
    lt_zero: int = Field(lt=0.0)
    lt_negative_2: int = Field(lt=-2.0)
    lt_negative_1: int = Field(lt=-1.0)

    # Le
    le_2: int = Field(le=2.0)
    le_1: int = Field(le=1.0)
    le_zero: int = Field(le=0.0)
    le_negative_2: int = Field(le=-2.0)
    le_negative_1: int = Field(le=-1.0)

    # Gt
    gt_2: int = Field(gt=2.0)
    gt_1: int = Field(gt=1.0)
    gt_zero: int = Field(gt=0.0)
    gt_negative_2: int = Field(gt=-2.0)
    gt_negative_1: int = Field(gt=-1.0)

    # Ge
    ge_2: int = Field(ge=2.0)
    ge_1: int = Field(ge=1.0)
    ge_zero: int = Field(ge=0.0)
    ge_negative_2: int = Field(ge=-2.0)
    ge_negative_1: int = Field(ge=-1.0)

    # Ge-Le
    ge_1_le_2: int = Field(ge=1.0, le=2.0)
    ge_0_le_1: int = Field(ge=1.0, le=2.0)
    ge_negative_2_le_negative_1: int = Field(ge=-2.0, le=-1.0)
    # Ge is Le
    # There is only one possible valid value for fields from here down.
    ge_1_le_1: int = Field(ge=1.0, le=1.0)
    ge_0_le_0: int = Field(ge=0.0, le=0.0)
    ge_negative_1_le_negative_1: int = Field(ge=-1.0, le=-1.0)
    ge_negative_2_le_negative_2: int = Field(ge=-2.0, le=-2.0)

    # Gt-Lt
    gt_1_lt_3: int = Field(gt=1.0, lt=3.0)
    gt_0_lt_2: int = Field(gt=0.0, lt=2.0)
    gt_negative_1_lt_1: int = Field(gt=-1.0, lt=1.0)
    gt_negative_2_lt_0: int = Field(gt=-2.0, lt=0.0)
    gt_negative_3_lt_negative_1: int = Field(gt=-3.0, lt=-1.0)

    # Ge-Lt
    ge_2_lt_3: int = Field(ge=2.0, lt=3.0)
    ge_1_lt_2: int = Field(ge=1.0, lt=2.0)
    ge_0_lt_1: int = Field(ge=0.0, lt=1.0)
    ge_negative_1_lt_0: int = Field(ge=-1.0, lt=0.0)
    ge_negative_2_lt_negative_1: int = Field(ge=-2.0, lt=-1.0)


class FloatMultipleOf(BaseModel):
    base: float = Field(multiple_of=2.5)
    # Gt/Ge
    gt: float = Field(multiple_of=2.5, gt=4.5)
    gt_match: float = Field(multiple_of=2.5, gt=5.0)
    ge: float = Field(multiple_of=2.5, ge=4.5)
    ge_match: float = Field(multiple_of=2.5, ge=5.0)
    # Lt/Le
    lt: float = Field(multiple_of=2.5, lt=2)
    lt_match: float = Field(multiple_of=2.5, lt=2.5)
    le: float = Field(multiple_of=2.5, le=2)
    le_match: float = Field(multiple_of=2.5, le=2.5)
    # Gt/Ge - Lt/Le
    gt_lt: float = Field(multiple_of=2.5, gt=7.0, lt=10.0)
    gt_lt_2: float = Field(multiple_of=2.5, gt=7.5, lt=11.0)
    ge_lt: float = Field(multiple_of=2.5, ge=7.5, lt=8.0)
    ge_lt_2: float = Field(multiple_of=2.5, ge=8.0, lt=11.0)
    gt_le: float = Field(multiple_of=2.5, gt=7.5, le=10.0)
    gt_le_2: float = Field(multiple_of=2.5, gt=7.0, le=8.0)
    ge_le: float = Field(multiple_of=2.5, ge=8.5, le=10.0)
    ge_le_2: float = Field(multiple_of=2.5, ge=7.5, le=8.0)


class PydanticTypes(BaseModel):
    strict_bool: StrictBool
    positive_int: PositiveInt
    negative_int: NegativeInt
    non_positive_int: NonPositiveInt
    non_negative_int: NonNegativeInt
    strict_int: StrictInt
    positive_float: PositiveFloat
    negative_float: NegativeFloat
    non_positive_float: NonPositiveFloat
    non_negative_float: NonNegativeFloat
    strict_float: StrictFloat
    finite_float: FiniteFloat
    strict_bytes: StrictBytes
    strict_str: StrictStr
    uuid1: UUID1
    uuid3: UUID3
    uuid4: UUID4
    uuid5: UUID5
    base64bytes: Base64Bytes
    base64str: Base64Str
    strict: Strict
    allow_inf_nan: AllowInfNan
    str_constraints_strip_whitespace: constr(strip_whitespace=True)  # type: ignore
    str_constraints_to_upper: constr(to_upper=True)  # type: ignore
    str_constraints_to_lower: constr(to_lower=True)  # type: ignore
    str_constraints_strict: constr(strict=True)  # type: ignore
    str_constraints_min_length: constr(min_length=10)  # type: ignore
    str_constraints_max_length: constr(max_length=1)  # type: ignore
    import_string: ImportString  # type: ignore
    json_field: Json  # type: ignore
    secret_str: SecretStr
    secret_bytes: SecretBytes
    bytesize: ByteSize
    bytesize_human_readable: ByteSize
    future_date: FutureDate
    aware_datetime: AwareDatetime
    naive_datetime: NaiveDatetime
    past_datetime: PastDatetime
    future_datetime: FutureDatetime


class PydanticNetworkTypes(BaseModel):
    any_url: AnyUrl
    any_http_url: AnyHttpUrl
    http_url: HttpUrl
    email_str: EmailStr
    name_email: NameEmail
    ipv_any_address: IPvAnyAddress
    ipv_any_interface: IPvAnyInterface
    ipv_any_network: IPvAnyNetwork


class PydanticExtra(BaseModel):
    color: Color
    payment_card_brand: PaymentCardBrand
    payment_card_number: PaymentCardNumber
    aba_routing_number: ABARoutingNumber


class EnumAsModelField(Enum):
    """Enum only used as a field of a model."""

    A = "A"
    B = "B"


class ModelUsingEnum(BaseModel):
    """Model using enum with string values."""

    vanilla_model: ForwardRefClass
    enum_field: EnumAsModelField


class NestedModels(BaseModel):
    """To test models with other models as fields."""

    name: str
    position: ForwardRefClass
    path: list[ForwardRefClass]
    recursion: typing.Optional["NestedModels"]
    list_recursion: typing.List[typing.Optional["NestedModels"]]
    any_of: typing.Union[ForwardRefClass, "NestedModels"]
    dict_model_values: dict[int, ModelUsingEnum] = Field(default_factory=dict)


Coffee.model_rebuild()
