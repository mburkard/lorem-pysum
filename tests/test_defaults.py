"""Test handling o fields with default values."""

from __future__ import annotations

from typing import Optional

from pydantic import BaseModel, Field

import lorem_pysum


class DefaultsModel(BaseModel):
    a: str = "2"
    b: int = 2
    c: float = 2.0
    d: bool = True
    e: bool = False
    f: Optional[list[str]] = None
    g: list[str] = Field(default_factory=lambda: ["Coffee"])


def test_defaults() -> None:
    model = lorem_pysum.generate(DefaultsModel, explicit_default=True)
    model_dump = model.model_dump(exclude_unset=True)
    assert model_dump["a"] == "2"
    assert model_dump["b"] == 2  # noqa: PLR2004
    assert model_dump["c"] == 2.0  # noqa: PLR2004
    assert model_dump["d"] is True
    assert model_dump["e"] is False
    assert model_dump["f"] is None
    assert model_dump["g"] == ["Coffee"]
