"""Test collections generation."""

from __future__ import annotations

import sys

from pydantic import BaseModel

import lorem_pysum


def test_collections() -> None:
    if sys.version_info < (3, 10):
        return

    class CollectionsModel(BaseModel):
        list_field: list  # type: ignore
        list_str: list[str]
        list_list: list[list]  # type: ignore
        list_list_int: list[list[int]]
        list_union: list[str | int]
        tuple_field: tuple  # type: ignore
        tuple_str: tuple[str]
        tuple_tuple: tuple[tuple]  # type: ignore
        tuple_tuple_int: tuple[tuple[int]]
        tuple_union: tuple[str | int]
        tuple_int_str_none: tuple[int, str, None]
        set_str: set[str]
        set_union: set[str | int]

    CollectionsModel(**lorem_pysum.generate(CollectionsModel).model_dump())
