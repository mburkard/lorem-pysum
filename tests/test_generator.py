"""Unit tests for model generator."""

import datetime
from _decimal import Decimal
from collections import OrderedDict
from uuid import uuid4

import pytest
from pydantic import BaseModel, Field, ValidationError

import lorem_pysum
from tests.models import (
    Brand,
    Coffee,
    Collections,
    Flavor,
    FloatMultipleOf,
    ForwardRefClass,
    IntLtLeGtGe,
)

default_string = "string"
default_date = datetime.date(year=1788, month=6, day=21)
default_time_delta = datetime.timedelta()
default_time = datetime.time()
default_list = [default_string]


def test_none() -> None:
    model = lorem_pysum.generate(Coffee, optionals_use_none=True)
    assert model.optional_str is None


def test_use_defaults_and_overrides() -> None:
    id_ = uuid4()
    assert lorem_pysum.generate(Coffee, use_default_values=False).id != id_
    assert lorem_pysum.generate(Coffee, overrides={"id": id_}).id == id_
    override_child_model_field_name = lorem_pysum.generate(
        Coffee, overrides={"str_field": "override"}
    )
    assert override_child_model_field_name.str_field == "override"
    assert override_child_model_field_name.forward_ref_field is not None
    assert override_child_model_field_name.forward_ref_field.str_field == default_string


def test_use_kwargs() -> None:
    brand = Brand(brand_name=str(uuid4()))
    assert lorem_pysum.generate(Coffee, overrides={"brand": brand}).brand == brand


def test_model_values() -> None:
    model = Coffee(**lorem_pysum.generate(Coffee).model_dump())

    assert model.optional_str == default_string
    assert model.bool_field is True
    assert model.enum_field == Flavor.MOCHA
    assert model.brand == Brand(brand_name=default_string)
    assert model.union == default_string
    assert model.always_none is None
    assert model.str_constraint_min == "s" * 101
    assert model.str_constraint_max == default_string
    assert model.str_constraint_minmax == "s" * 101
    assert model.date_field == default_date
    assert model.time_field == default_time
    assert model.timedelta_field == default_time_delta
    assert model.datetime_field == datetime.datetime.fromordinal(
        default_date.toordinal()
    )
    assert model.not_specifically_supported_type == OrderedDict()  # type: ignore
    assert model.forward_ref_field == ForwardRefClass(str_field=default_string)
    assert model.any_type == {}
    assert model.recursion is None
    assert model.recursion_union == ForwardRefClass(str_field=default_string)
    assert model.recursion_union_two == ForwardRefClass(str_field=default_string)
    assert model.optional_default_none is None
    assert model.int_field == 1
    assert model.float_field == 1.0
    assert model.dec == Decimal("1.0")
    assert model.const == "mocha"


def test_collections() -> None:
    model = Collections(**lorem_pysum.generate(Collections).model_dump())

    assert model.set_any == {"any"}  # type: ignore
    assert model.set_explicit_any == {"any"}
    assert model.set_int == {1}
    assert model.tuple_any == ({},)  # type: ignore
    assert model.tuple_explicit_any == ({},)
    assert model.tuple_int == (1,)
    assert model.tuple_int_str_any == (1, default_string, {})
    assert model.list_any == [{}]  # type: ignore
    assert model.list_explicit_any == [{}]
    assert model.list_str == default_list
    assert model.list_of_lists == [default_list]
    assert model.constrained_list == ["s" * 10 for _ in range(10)]
    assert model.constrained_list_union == ["s" * 10 for _ in range(10)]
    assert model.constrained_list_unique == {1}
    assert model.dictionary == {default_string: [1]}
    assert model.untyped_dict == {}  # type: ignore


def test_integers() -> None:
    model = IntLtLeGtGe(**lorem_pysum.generate(IntLtLeGtGe).model_dump())

    assert model.lt_2 == 1
    assert model.lt_1 == 0
    assert model.lt_zero == -1
    assert model.lt_negative_2 == -3  # noqa: PLR2004
    assert model.lt_negative_1 == -2  # noqa: PLR2004

    assert model.le_2 == 1
    assert model.le_1 == 1
    assert model.le_zero == 0
    assert model.le_negative_2 == -2  # noqa: PLR2004
    assert model.le_negative_1 == -1

    assert model.gt_2 == 4  # noqa: PLR2004
    assert model.gt_1 == 3  # noqa: PLR2004
    assert model.gt_zero == 1
    assert model.gt_negative_2 == 1
    assert model.gt_negative_1 == 1

    assert model.ge_2 == 3  # noqa: PLR2004
    assert model.ge_1 == 1
    assert model.ge_zero == 1
    assert model.ge_negative_2 == 1
    assert model.ge_negative_1 == 1

    assert model.ge_1_le_2 == 1
    assert model.ge_0_le_1 == 1


def test_floats() -> None:
    model = FloatMultipleOf(**lorem_pysum.generate(FloatMultipleOf).model_dump())

    assert model.base == 5.0  # noqa: PLR2004
    assert model.gt == 7.5  # noqa: PLR2004
    assert model.gt_match == 10.0  # noqa: PLR2004
    assert model.ge == 7.5  # noqa: PLR2004
    assert model.ge_match == 7.5  # noqa: PLR2004
    assert model.lt == 0.0
    assert model.lt_match == 0.0
    assert model.le == 0.0
    assert model.le_match == 2.5  # noqa: PLR2004


def test_invalid_values() -> None:
    # These are the conditions in which we give up on matching the model.
    class CruelModel(BaseModel):
        string: str = Field(pattern=str(uuid4()))

    model = lorem_pysum.generate(CruelModel)
    with pytest.raises(ValidationError):
        CruelModel(**model.model_dump())
