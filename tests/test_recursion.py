"""Test recursive model."""

from __future__ import annotations

from typing import Optional

from pydantic import BaseModel

import lorem_pysum


class RecursiveModel(BaseModel):
    recursive: Optional[RecursiveModel]
    recursive_list: list[RecursiveModel]
    recursive_dict: dict[str, RecursiveModel]


def test_none() -> None:
    model = lorem_pysum.generate(RecursiveModel)
    assert model.recursive is None
    assert model.recursive_list == []
    assert model.recursive_dict == {}
