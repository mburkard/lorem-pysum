"""Test Pydantic special types."""

from py_undefined import Undefined
from pydantic import BaseModel, FilePath, PastDate
from pydantic_extra_types import phone_numbers

import lorem_pysum
from tests.models import PydanticExtra, PydanticNetworkTypes, PydanticTypes


def test_pydantic_types() -> None:
    PydanticTypes(**lorem_pysum.generate(PydanticTypes).model_dump())
    assert True


def test_pydantic_network_types() -> None:
    PydanticNetworkTypes.model_validate(
        lorem_pysum.generate(PydanticNetworkTypes).model_dump()
    )
    assert True


def test_pydantic_extra_types() -> None:
    PydanticExtra(**lorem_pysum.generate(PydanticExtra).model_dump())
    assert True


def test_past_date() -> None:
    class PDModel(BaseModel):
        past_date: PastDate

    PDModel.model_validate(lorem_pysum.generate(PDModel))


def test_phone_number() -> None:
    class PhoneModel(BaseModel):
        field: phone_numbers.PhoneNumber

    PhoneModel.model_validate(lorem_pysum.generate(PhoneModel))


def test_files() -> None:
    class FilesModel(BaseModel):
        field2: FilePath

    FilesModel.model_validate(lorem_pysum.generate(FilesModel))


def test_undefined() -> None:
    class UndefinedModel(BaseModel):
        field: Undefined

        class Config:
            arbitrary_types_allowed = True

    UndefinedModel.model_validate(lorem_pysum.generate(UndefinedModel))
